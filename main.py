# -*- coding: UTF-8 -*-
from selenium import webdriver
from bs4 import BeautifulSoup
import chromedriver_autoinstaller
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy import create_engine
import time
import random
import os
import json
import re
from relation import *


def cut(s):
    return s[s.find(')') + 1:]


def webdriverSetup():

    global driver
    global chrome_option

    chromedriver_autoinstaller.install()
    chrome_option = webdriver.ChromeOptions()
    chrome_option.add_argument("--disable-blink-features=AutomationControlled")
    chrome_option.add_argument("--window-size=1920,1080")
    chrome_option.add_argument("--headless=new")
    chrome_option.add_argument("--disable-gpu")
# for docker
    chrome_option.add_argument("--no-sandbox")
    chrome_option.add_argument("--disable-setuid-sandbox")
# ---
    driver = webdriver.Chrome(options=chrome_option)


def scrapeAllCourse(sess):
    idSet = set()
    driver.get("http://course.ncku.edu.tw/index.php?c=qry11215&m=en_query")
    for i in range(1, 8):
        sel = Select(driver.find_element(By.XPATH,"//select[@id='sel_wk']"))
        qryBtn = driver.find_element(By.XPATH,'//button[@onclick="setdata()"]')
        sel.select_by_value(str(i))
        qryBtn.click()
        WebDriverWait(driver, 30).until(EC.invisibility_of_element_located(
            (By.XPATH, '//div[@class="block_circle"]')))
        
        soup = BeautifulSoup(driver.page_source, "lxml")
        trList = soup.select("div#result table tbody tr")
        newCourses = []
        for tr in trList:
            try:
                course_a = tr.select_one('span.course_name a')
                courseLink = course_a['href'] if course_a else None
                courseName = course_a.get_text().strip() if course_a else None
                courseID = tr.select_one('td:nth-of-type(2) div').get_text().strip()
                courseMand = tr.select_one('td:nth-of-type(6)').get_text().strip()
                courseDeptElement = tr.select_one('td:nth-of-type(1)')
                courseDept = courseDeptElement.get_text().strip() if courseDeptElement else None
                courseDept = re.compile('[^a-zA-Z ]*').match(courseDept).group()

            except Exception as e:
                continue
            if not all([course_a, courseLink, courseName, courseID, courseDept, courseMand]):
                continue
            if courseID in idSet:
                continue
            
            courseMand = bool(re.compile("[0-9]*[ ]*必修[ ]*").match(courseMand))

            idSet.add(courseID)
            newCourses.append(Course(id=courseID,
                                deptName=courseDept,
                                name=courseName,
                                link=courseLink,
                                mandatoriness=courseMand))
            
            #print(f'({courseID}, {courseDept}, {courseName}, {courseLink}, {courseMand})')
        
        try:
            sess.add_all(newCourses)
        except Exception:
            raise
        
        
        if i < 7:
            time.sleep(0.5 + random.random())

def scrapeDept(sess):
    driver.get("https://course.ncku.edu.tw/index.php?c=qry_all")
    langBtn = driver.find_element(By.XPATH,
        """//a[@onclick="javascript:setLang('cht');return false;"]""")
    langBtn.click()
    collList = driver.find_elements(By.XPATH,
        '//div[@class="panel-group hidden-xs"]/div')
    i = 0
    for coll in collList:
        deptList = coll.find_elements(By.XPATH,'.//li[@class="btn_dept"]')
        collName = coll.find_element(By.XPATH,
            './/div[@class="panel-heading"]').get_attribute("textContent")
        deptIDList = []
        for dept in deptList:
            deptEntry = cut(dept.get_attribute("textContent"))
            deptID = dept.get_attribute("data-dept")
            deptName = deptEntry[:deptEntry.find(" ")]
            deptAbber = deptEntry[deptEntry.find(" ") + 1:]
            
            try:
                sess.add(Dept(id=deptID, name=deptName, abber=deptAbber))
            except Exception:
                raise

            deptIDList.append(deptID)
        try:
            sess.add(College(id=i, name=collName, depts=json.dumps(deptIDList)))
            i+=1
        except Exception:
            raise

if __name__ == "__main__":
    webdriverSetup()
    if os.path.isfile("./db.sqlite"):
        os.remove("./db.sqlite")
    engine = create_engine(
        "sqlite+pysqlite:///db.sqlite", echo=True, future=True)
    Base.metadata.create_all(engine)
    Session = sessionmaker(engine)
    with Session() as s:
        scrapeDept(s)
        scrapeAllCourse(s)
        s.commit()
    driver.quit()
