from sqlalchemy import Column, Integer, String, JSON, Boolean
from base import Base
from dataclasses import dataclass


@dataclass
class Dept(Base):
    __tablename__ = 'dept'

    id: str
    name: str
    abber: str

    id = Column(String, primary_key=True)
    name = Column(String)
    abber = Column(String)


@dataclass
class College(Base):
    __tablename__ = 'college'

    id: int
    name: str
    depts: JSON

    id = Column(Integer, primary_key=True)
    name = Column(String)
    depts = Column(JSON)

@dataclass
class Course(Base):
    __tablename__ = 'course'

    id: str #課程碼 in 1st td (starting from 0)
    deptName : str #系所名 in 0th td
    name: str #科目名稱 in 4th td
    link: str #課綱連結 in 4th td
    mandatoriness = bool #選必修 in 5th td

    id = Column(String, primary_key=True)
    deptName = Column(String)
    name = Column(String)
    link = Column(String)
    mandatoriness = Column(Boolean)
